from Part1 import short, long
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt

import numpy as np

# Writing the conditions of the bonds
if __name__ == '__main__':
    print("calculation of your investment; input values below")
    p = int(input("enter the amount to invest"))#ASKING THE USER TO IMPUT AMOUNT TO INVEST
    m = int(input("enter the term of maturity of investment(in years)"))#ASKING THE USER TO IMPUT TERM OF INVESTMENT

    if (m >= 2) & (m < 5) & (p >= 250):#CONDITIONS

        FV, compounding = short(p, m).FVcomputation()
        print(" Future Value of the investment is " + str(round(FV, 3)) + " compound interest is " + str(
            round(compounding, 3)))

    elif (m >= 5) & (p >= 1000):
        FV, compounding = long(p, m).FVcomputation()
        print(" Future Value of the investment is " + str(round(FV, 3)) + " compound interest is " + str(
            round(compounding, 3)))

    else:
        print("try again the input must correspond to either short or long characteristics")

    # the plot
    print(
        "this is the plot of the evolution of the investment of the minimum allowed invested amount for both bonds over a period of 50 years.")
    Years = range(50)
    Sfunction = [(250 * (1 + 0.015) ** i) for i in Years]
    Lfunction = [(1000 * (1 + 0.03) ** i) for i in Years]
    plt.plot(Sfunction, color="green")  # the short term bond plot over the years is green
    plt.plot(Lfunction, color="red")  # the long term bond plot over the years is red
    plt.grid(True)
    plt.title('Evolution of minimun investment over 50 years')
    plt.legend("LS")
    plt.xlabel("Years")
    plt.ylabel("Future values")
    plt.show()

#finished