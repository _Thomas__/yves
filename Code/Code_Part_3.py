import matplotlib.pyplot as plt
import numpy as np
import random
import investpy
from random import choice
import math
import pandas as pd
import pandas_datareader.data as web

tickers = ['FDX', 'GOOGL', 'IBM', 'KO', 'MS', 'NOK', 'XOM']
dataframe = pd.DataFrame()
for t in tickers:
    dataframe[t] = web.DataReader(t, data_source="yahoo", start="2016-9-1", end="2021-1-1")["High"]
# print(dataframe)
matrix_of_stocks = np.array(dataframe).reshape(7, 1091)
# print(matrix_of_stocks)
for aggressive_investor in range(1, 501):
    i = random.randint(0, 1090)  # RANDOM NUMBER OF PRICES
    t = random.choice(tickers)  # RANDOM CHOICE OF TICKER
    random_nb1 = random.randint(0, 6)
    starting_stock_price = dataframe[t][i]

    nb_of_stock_purchase1 = math.floor(5000 / starting_stock_price)  # THE NUMBER OF STOCKS BOUGHT FORMULA
    print("The Total number of stocks bought is  " + str(nb_of_stock_purchase1))

    remainning_money1 = 5000 - starting_stock_price * nb_of_stock_purchase1

    if remainning_money1 < 100:
        print("This aggressive investor has purchased ", nb_of_stock_purchase1, "of stocks", nb_of_stock_purchase1,
              "for",
              starting_stock_price, "per share")

        aggressive_table = matrix_of_stocks[random_nb1] * nb_of_stock_purchase1

    else:

        i2 = random.randint(0, 1090)#NEW RANDOM NUMBER GENRATION FOR PRICE
        t2 = random.choice(tickers)  # RANDOM CHOICE OF TICKER
        starting_mixed_price_of_random_stock2 = dataframe[t2][i2]
        # print(start_price2)
        nb_of_stock_purchase2 = int(
            remainning_money1 / starting_mixed_price_of_random_stock2)  # THE NUMBER OF STOCKS BOUGHT FORMULA
        remainning_money2 = remainning_money1 - starting_mixed_price_of_random_stock2 * nb_of_stock_purchase2

        print("This aggressive investor has purchased ", nb_of_stock_purchase1, "of stock ", i2, " for ",
              starting_mixed_price_of_random_stock2, "per share, and ", nb_of_stock_purchase2, " of stock ",
              " for ", starting_mixed_price_of_random_stock2)
        random_nb2 = random.randint(0, 6)
        aggressive_table = matrix_of_stocks[random_nb1] * nb_of_stock_purchase1 + matrix_of_stocks[
            random_nb2] * nb_of_stock_purchase2
        # print(aggressive_table)

# Plot of 500 aggressive investors
plt.plot(aggressive_table, label='aggressive investor')
plt.grid(True)
plt.title('Plot of 500 aggressive investors from 01/09/2016 to 01/01/2021')
plt.xlabel('Years from 01/09/2016')
plt.ylabel('Stock price')
plt.show()

# Model 500 defensive investors with a starting budget of 5000.
#Since we are going to compare the returns of different types of investors later,thus defensive investors must have the same investement timeseries as other investors
#Thus here the duration of investment is from a period of 09/01/2016 to 01/01/2021

for defensive_investor in range(1, 501):
    # THE PRICE AND RATE OF THE BOND
    s = 250
    rs = 0.015
    # THE PRICE AND RATE OF THE BOND
    l = 1000
    rl = 0.03
    risk_free = 0.0113

    nb_of_short_term_bond1 = int(2500 / s) # the money for each bond types is 5000/2=2500

    nb_of_long_term_bond = int(2500 / l)# number of long term bond purchased

    remainning_bond_money = 2500 - nb_of_long_term_bond * l # remainning money after long term bond purchased

    nb_of_short_term_bond2 = int(remainning_bond_money / s)# repurchase short term bond with remaining monet

    total_nb_of_short_term_bond = nb_of_short_term_bond1 + nb_of_short_term_bond2
    print("This defensive investor has purchased " + str(total_nb_of_short_term_bond) + " of short bonds and " + str(
        nb_of_long_term_bond) + " of long bonds.")

    # From this proceed we know that all defensive investors will have 12 shorst term bonds and 2 long term bonds
    # Caculate the value of investment at the beginning
    beginning_bond_investment = 12 * s * (1 + rs) ** 2 + 2 * l * (1 + rl) ** 5

    # count the real number of days
    import datetime as dt
    from datetime import date
    d0 = date(2016, 9, 1)
    d1 = date(2021, 1, 1)
    real_date = d1 - d0
    nb_real_date = real_date.days
    nb_of_real_days_array = range(nb_real_date)

    # caculate of bond price and record it on the table
    defensive_bond_list = [beginning_bond_investment * (1 + risk_free) ** (i / 365) for i in range(nb_real_date)]
    defensive_table = defensive_bond_list

# plot the defensive investors
plt.plot(defensive_table, label='defensive investor')
plt.title('Plot of 500 defensive investors from 01/09/2016 to 01/01/2021')
plt.xlabel('Number of business days start from 01/09/2016')
plt.ylabel('Bond price')
plt.show()

# Model 500 mixed investors with a starting budget of 5000.
# mixed investors buy25% bond with 75% stocks

# create a table of 500 mixted investors

for mixed_investor in range(1, 501):# the money for each bond types is 5000*25%/2=625 and $625 is not enough even for buying one long term bond
    # thus mixed investors will only buy short term bond

    nb_of_mixed_short_term_bond = int(625 * 2 / s)
    beginning_mixed_bond_investment = (5 * s) * (1 + rs) ** 2 # from previous compute we know that the mixed investor will only purchase 5 short term bonds
    mixed_of_bond_investment = [beginning_mixed_bond_investment * (1 + risk_free) ** (i / 365) for i in range(0, 1091)]
    random_mixed_nb1 = random.randint(0, 6)

    starting_mixed_price_of_random_stock1 = starting_stock_price# find the starting price for the random selected stocks

    nb_of_mixed_stock_purchase1 = math.floor(3750 / starting_mixed_price_of_random_stock1)# caculte the number of stock purchase

    remainning_mixed_money1 = 5000 - starting_mixed_price_of_random_stock1 * nb_of_mixed_stock_purchase1# caculate the remainning  money after investment

    if remainning_mixed_money1 < 100:
        print("This mixed investor purchase ", nb_of_mixed_stock_purchase1, "of stock", random_mixed_nb1, "for $",
              starting_mixed_price_of_random_stock1, "per share and", nb_of_mixed_short_term_bond,
              "of  short term bonds for $", s, "per share")

        mixed_table = matrix_of_stocks[random_mixed_nb1] * nb_of_mixed_stock_purchase1 + mixed_of_bond_investment

    else:# pick another stock

        random_mixed_nb2 = random.randint(0, 6)

        starting_mixed_price_of_random_stock2 = starting_stock_price

        nb_of_mixed_stock_purchase2 = int(remainning_mixed_money1 / starting_mixed_price_of_random_stock2)
        remainning_mixed_money2 = remainning_mixed_money1 - starting_mixed_price_of_random_stock2 * nb_of_mixed_stock_purchase2
        print("This mixed investor purchase ", nb_of_mixed_stock_purchase1, "of stock", random_mixed_nb1, "for",
              starting_mixed_price_of_random_stock1, "per share, and", nb_of_mixed_stock_purchase2, "of stock",
              random_mixed_nb2, "for $",
              starting_mixed_price_of_random_stock2, "per share and", nb_of_mixed_short_term_bond,
              "of  short term bonds for $", s, "per share")

        # compute the mixed investment price and add it on the table
        mixed_table = matrix_of_stocks[random_mixed_nb1] * nb_of_mixed_stock_purchase1 + matrix_of_stocks[
            random_mixed_nb2] * nb_of_mixed_stock_purchase2 + mixed_of_bond_investment

# print(mixed_table)
# Plot of 500 aggressive investors
plt.plot(mixed_table, label='aggressive investor')
plt.title('Plot of 500 mixed investors from 01/09/2016 to 01/01/2021')
plt.xlabel('Number of business days start from 01/09/2016')
plt.ylabel('Price of financial instruments')
plt.show()

#finished